import './index.scss';
import osjs from 'osjs';
import {name as applicationName} from './metadata.json';
import Vue from 'vue';
import App from "./src/App.vue";
import {Terminal} from 'xterm';
import {FitAddon} from "xterm-addon-fit";
import {AttachAddon} from "xterm-addon-attach"

// Our launcher
const register = (core, args, options, metadata) => {
    const term = new Terminal();

    // Create a new Application instance
    const proc = core.make('osjs/application', {args, options, metadata});




    const render = ($content) => {
        term.open($content);

        $content.addEventListener('contextmenu', ev => {
            ev.preventDefault();

            core.make('osjs/contextmenu', {
                position: ev,
                menu: [{
                    label: 'Copy text selection',
                    onclick: () => clipboard.writeText(
                        term.getSelection()
                    )
                }, {
                    label: 'Paste from clipboard',
                    onclick: () => clipboard.readText()
                        .then(data => term.write(data))
                }]
            });
        });
    };

    const fit = () => {
        setTimeout(() => {
        }, 100);
    };




    // Create  a new Window instance
    proc.createWindow({
        id: 'TerminalWindow',
        title: metadata.title.en_EN,
        dimension: {width: 400, height: 400},
        position: {left: 700, top: 200}
    })
        .on('resized', fit)
        .on('maximize', fit)
        .on('restore', fit)
        .on('moved', () => term.focus())
        .on('focus', () => term.focus())
        .on('blur', () => term.blur())
        .on('destroy', () => proc.destroy())
        .render(render);

    // Creates a new WebSocket connection (see server.js)
    //const sock = proc.socket('/socket');
    //sock.on('message', (...args) => console.log(args))
    //sock.on('open', () => sock.send('Ping'));

    // Use the internally core bound websocket
    //proc.on('ws:message', (...args) => console.log(args))
    //proc.send('Ping')

    // Creates a HTTP call (see server.js)
    //proc.request('/test', {method: 'post'})
    //.then(response => console.log(response));

    return proc;
};

// Creates the internal callback function when OS.js launches an application
osjs.register(applicationName, register);
