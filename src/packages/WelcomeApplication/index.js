import './index.scss';
import osjs from 'osjs';
import {name as applicationName} from './metadata.json';
import Vue from 'vue';
import App from './src/App.vue';


// Our launcher
const register = (core, args, options, metadata) => {
  // Create a new Application instance
  const proc = core.make('osjs/application', {args, options, metadata});

  // Create  a new Window instance
  proc.createWindow({
    id: 'WelcomeApplicationWindow',
    title: metadata.title.en_EN,
    dimension: {width: 400, height: 400},
    position: "center"
  })
    .on('destroy', () => proc.destroy())
    .render($content => {
      new Vue({
        el: $content,
        render: h => h(App),
        data: function() {
          return {
            proc: proc,
            core: core
          }
      }
      });
    });


  return proc;
};

// Creates the internal callback function when OS.js launches an application
osjs.register(applicationName, register);
